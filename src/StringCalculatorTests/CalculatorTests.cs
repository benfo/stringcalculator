﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringCalculator;

namespace StringCalculatorTests
{
    [TestClass]
    public class CalculatorTests
    {
        private Calculator calculator;

        [TestInitialize]
        public void Init()
        {
            calculator = new Calculator();
        }

        [TestMethod]
        public void TestAddWithCommaDelimiters()
        {
            var result = calculator.Add("1,2,3");

            Assert.AreEqual(6, result);
        }

        [TestMethod]
        public void TestAddWithHashDelimiters()
        {
            var result = calculator.Add("1#2#3#4");

            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public void TestAddWithMixedDefaultDelimiters()
        {
            var result = calculator.Add("1#2,3#4,5");

            Assert.AreEqual(15, result);
        }

        [TestMethod]
        public void TestAddWithEmptyInputShouldReturnZero()
        {
            var result = calculator.Add(string.Empty);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void TestAddWithNoDelimiters()
        {
            var result = calculator.Add("120");

            Assert.AreEqual(120, result);
        }

        [TestMethod]
        public void TestAddWithOneCustomDelimiter()
        {
            var result = calculator.Add("//~#5~15~100");

            Assert.AreEqual(120, result);
        }

        [TestMethod]
        public void TestAddWithOneCustomDelimiterMixedWithDefaultDelimiters()
        {
            var result = calculator.Add("//~#25#50~25,25");

            Assert.AreEqual(125, result);
        }

        [TestMethod]
        public void TestAddWithMultipleCustomDelimitersMixedDelimiters()
        {
            var result = calculator.Add("//[~][$][^^][**]#5~15$100**200^^400");

            Assert.AreEqual(720, result);
        }

        [TestMethod]
        [ExpectedException(typeof(NegativeNumberException))]
        public void TestAddWithNegativeNumbersShouldThrowException()
        {
            calculator.Add("-5,-7");
        }

        [TestMethod]
        public void TestMessageOfAddWithNegativeNumbersShouldThrowException()
        {
            try
            {
                calculator.Add("-5,-7");
            }
            catch (NegativeNumberException ex)
            {
                Assert.AreEqual("Negatives not allowed: -5, -7 ...", ex.Message);
            }
        }

        [TestMethod]
        public void TestAddIgnoresNumbersBiggerThan1000()
        {
            var result = calculator.Add("1001,5,5,2000");

            Assert.AreEqual(10, result);            
        }
    }
}
