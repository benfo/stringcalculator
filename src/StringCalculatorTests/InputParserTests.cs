﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringCalculator;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculatorTests
{
    [TestClass]
    public class InputParserTests
    {
        [TestMethod]
        public void TestParseWithNoDelimiterDefinition()
        {
            var parser = InputParser.Parse("1,2#3#4,5");

            AssertArrayValuesAreEqual(parser.Delimiters, new[] { "#", "," });
            CollectionAssert.AreEqual(new[] { 1, 2, 3, 4, 5 }, parser.Numbers);
        }

        [TestMethod]
        public void TestParseWithOneDelimiterDefinition()
        {
            var parser = InputParser.Parse("//~#5~15~100");

            AssertArrayValuesAreEqual(new[] { "~", "#", "," }, parser.Delimiters);
            CollectionAssert.AreEqual(new[] { 5, 15, 100 }, parser.Numbers);
        }

        [TestMethod]
        public void TestParseWithMultipleSingleDigitDelimiterDefinitions()
        {
            var parser = InputParser.Parse("//[~][$]#5~15$100");

            AssertArrayValuesAreEqual(new[] { "~", "#", ",", "$" }, parser.Delimiters);
            CollectionAssert.AreEqual(new[] { 5, 15, 100 }, parser.Numbers);
        }

        [TestMethod]
        public void TestParseWithMultipleSingleAndDoubleDigitDelimiterDefinitions()
        {
            var parser = InputParser.Parse("//[~][$][^^][**]#5~15$100**200^^400");

            AssertArrayValuesAreEqual(new[] { "~", "#", ",", "$", "^^", "**" }, parser.Delimiters);
            CollectionAssert.AreEqual(new[] { 5, 15, 100, 200, 400 }, parser.Numbers);
        }

        private static void AssertArrayValuesAreEqual<T>(IEnumerable<T> arr1, IEnumerable<T> arr2)
        {
            Assert.AreEqual(0, arr1.Except(arr2).Count(), "Not all values in both arrays are the same.");
        }
    }
}