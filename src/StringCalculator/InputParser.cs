﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
    public class InputParser
    {
        private static readonly string[] DefaultDelimiters = { ",", "#" };
        private const string DelimiterDefinitionStart = "//";
        private const string DelimiterDefinitionEnd = "#";
        private bool _hasDelimiterDefinition;
        private string _input;

        private InputParser()
        {
        }

        public string[] Delimiters { get; private set; }

        public int[] Numbers { get; private set; }

        public static InputParser Parse(string input)
        {
            var parser = new InputParser();
            parser.PerformParsing(input);
            return parser;
        }

        private void PerformParsing(string input)
        {
            _hasDelimiterDefinition = input.StartsWith(DelimiterDefinitionStart);
            _input = input;

            ExtractDelimiters();
            ExtractNumbers();
        }

        private void ExtractDelimiters()
        {
            var delimiters = DefaultDelimiters.AsEnumerable();

            if (_hasDelimiterDefinition)
            {
                var delimiterValue = ExtractDelimiterValue();
                var newDelimiters = BuildDelimiterList(delimiterValue);

                delimiters = delimiters.Union(newDelimiters);
            }

            Delimiters = delimiters.ToArray();
        }

        private static IEnumerable<string> BuildDelimiterList(string delimiterValue)
        {
            string[] delimiterList;

            if (ContainsMultipleDelimiters(delimiterValue))
            {
                delimiterList = delimiterValue
                    .TrimStart('[').TrimEnd(']')
                    .Split(new[] { "][" }, StringSplitOptions.None);
            }
            else
            {
                delimiterList = new[] { delimiterValue };
            }

            return delimiterList;
        }

        private static bool ContainsMultipleDelimiters(string delimiterValue)
        {
            return delimiterValue.StartsWith("[")
                   && delimiterValue.EndsWith("]")
                   && delimiterValue.Length > 2;
        }

        private string ExtractDelimiterValue()
        {
            var startPosition = DelimiterDefinitionStart.Length;
            var endPosition = GetNumbersStartPosition() - 1;

            var delimiterString = _input.Substring(startPosition, endPosition - startPosition);
            return delimiterString;
        }

        private void ExtractNumbers()
        {
            var numbers = _input.Substring(GetNumbersStartPosition());

            Numbers = numbers
                .Split(Delimiters, StringSplitOptions.None)
                .Select(int.Parse)
                .ToArray();
        }

        private int GetNumbersStartPosition()
        {
            return _hasDelimiterDefinition
                ? _input.IndexOf(DelimiterDefinitionEnd, StringComparison.Ordinal) + 1
                : 0;
        }
    }
}