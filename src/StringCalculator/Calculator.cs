﻿using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
    public class Calculator
    {
        private const int MaxValue = 1000;

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var parser = InputParser.Parse(numbers);

            ValidateNumbers(parser.Numbers);

            return parser
                .Numbers
                .Where(n => n < MaxValue)
                .Sum();
        }

        private void ValidateNumbers(IEnumerable<int> numbers)
        {
            var negatives = numbers.Where(n => n < 0);
            if (negatives.Any())
            {
                string message = string.Format("Negatives not allowed: {0} ...", string.Join(", ", negatives));
                throw new NegativeNumberException(message);
            }
        }
    }
}